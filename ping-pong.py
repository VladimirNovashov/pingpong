import pygame
from random import randint

pygame.init()
pygame.mixer.init()

sound = pygame.mixer.Sound('ping.wav')

screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)  # Creating a window
pygame.display.set_caption('PING-PONG')
sc_width = screen.get_width()
sc_height = screen.get_height()


class Player:
    def __init__(self, side):
        self.width = sc_width // 250
        self.height = sc_height // 4
        self.side = side
        if side == 'right':
            self.x = sc_width - self.width - 0
        elif side == 'left':
            self.x = 0
        self.y = sc_height // 2 - self.height // 2
        self.speed = sc_width // 200
        self.RGB = [255, 255, 255]
        self.score = 0

    def move(self):
        key = pygame.key.get_pressed()
        if self.side == 'left':
            if key[pygame.K_q] and self.y >= 0:
                self.y -= self.speed
            if key[pygame.K_a] and self.y + self.height <= sc_height:
                self.y += self.speed
        if self.side == 'right':
            if key[pygame.K_p] and self.y >= 0:
                self.y -= self.speed
            if key[pygame.K_l] and self.y + self.height <= sc_height:
                self.y += self.speed

        pygame.draw.rect(screen, self.RGB, (self.x, self.y, self.width, self.height))

    def spawn(self):
        pygame.draw.rect(screen, self.RGB, (self.x, self.y, self.width, self.height))


class Ball:
    def __init__(self):
        self.radius = sc_width // 40 // 2
        self.x = sc_width // 2
        self.y = sc_height // 2
        self.speedX = [sc_height // 100, -sc_height // 100][randint(0, 1)]
        self.speedY = randint(-sc_height // 100, sc_height // 100)
        self.RGB = [255, 255, 255]
        self.mSpeed = sc_width // 100

    def move(self, *objects):
        if self.y - self.radius <= 0:
            self.speedY = randint(1, self.mSpeed)
        elif self.y + self.radius >= sc_height:
            self.speedY = randint(-self.mSpeed, -1)

        for o in objects:
            if o.side == 'right':
                if self.x + self.radius >= o.x:
                    if (o.y <= self.y) and (o.y + o.height >= self.y):
                        sound.play()
                        self.speedX = -self.speedX
                        self.speedY = randint(1, self.mSpeed)
                        if o.speed != self.mSpeed // 2:
                            o.speed -= 1
                        o.score += 1
            elif o.side == 'left':
                if self.x - self.radius <= o.x + o.width:
                    if (o.y <= self.y) and (o.y + o.height >= self.y):
                        sound.play()
                        self.speedX = -self.speedX
                        self.speedY = randint(-self.mSpeed, -1)
                        if o.speed != self.mSpeed // 2:
                            o.speed -= 1
                        o.score += 1

        self.x += self.speedX
        self.y += self.speedY

        pygame.draw.circle(screen, self.RGB, (self.x, self.y), self.radius)

    def spawn(self):
        pygame.draw.circle(screen, self.RGB, (self.x, self.y), self.radius)


class Line:
    def __init__(self):
        self.width = sc_width // 650
        self.RGB = [255, 255, 255]

    def spawn(self):
        pygame.draw.line(screen, self.RGB, (sc_width // 2, 0), (sc_width // 2, sc_height), self.width)


class Text:
    def __init__(self, size, side):
        self.size = sc_width // 13
        self.side = side
        self.RGB = [255, 255, 255]
        self.x = None
        self.text = pygame.font.SysFont(None, self.size).render('0', 1, self.RGB)
        self.y = sc_height // 3 - self.text.get_height() // 2

    def spawn(self, text):
        self.text = pygame.font.SysFont(None, self.size).render(str(text), 1, self.RGB)
        if self.side == 'right':
            self.x = sc_width - sc_width // 3 - self.text.get_width() // 2
        elif self.side == 'left':
            self.x = sc_width // 3 - self.text.get_width() // 2
        screen.blit(self.text, (self.x, self.y))


class Button:
    def __init__(self, y, text, command):
        self.width = sc_width // 4
        self.height = sc_height // 10
        self.x = sc_width // 2 - self.width // 2
        self.y = y
        self.RGB_rect = [255, 255, 255]
        self.RGB_text = [0, 0, 0]
        self.size = sc_width // 17
        self.text = pygame.font.SysFont(None, self.size).render(text, 1, self.RGB_text)
        self.command = command

    def spawn(self):
        pygame.draw.rect(screen, self.RGB_rect, (self.x, self.y, self.width, self.height))
        screen.blit(self.text, (
            self.x + self.width // 2 - self.text.get_width() // 2,
            self.y + self.height // 2 - self.text.get_height() // 2))

    def manager(self):
        self.command()


run = True
run_menu = True
run_game = False


def start_game():
    global run_menu
    global run_game
    run_menu = False
    run_game = True


def end_game():
    global run
    global run_menu
    global run_game
    run = False
    run_menu = False
    run_game = False


while run:
    bSt = Button(sc_height // 2.4, 'Start', start_game)
    bEt = Button(sc_height - sc_height // 2.4, 'Exit', end_game)
    text = pygame.font.SysFont(None, sc_height // 5).render('PING-PONG 90-x', 1, [255, 255, 255])
    while run_menu:  # Menu
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                x = event.pos[0]
                y = event.pos[1]
                if (x > bSt.x) and (x < bSt.x + bSt.width) and (y > bSt.y) and (y < bSt.y + bSt.height):
                    bSt.command()
                if (x > bEt.x) and (x < bEt.x + bEt.width) and (y > bEt.y) and (y < bEt.y + bEt.height):
                    bEt.command()

        bSt.spawn()
        bEt.spawn()

        screen.blit(text, (sc_width // 2 - text.get_width() // 2, sc_height // 5 - text.get_height() // 2))

        pygame.display.update()
        screen.fill((0, 0, 0))
        pygame.time.Clock().tick(5)

    pl = Player('left')
    pr = Player('right')
    cr = Ball()
    li = Line()
    sr = Text(100, 'right')
    sl = Text(100, 'left')
    first_game = True
    while run_game:  # Game
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                run_game = False
                run_menu = True

        if first_game:
            pl.spawn()
            pr.spawn()
            cr.spawn()
            li.spawn()
            sr.spawn(pr.score)
            sl.spawn(pl.score)
            pygame.display.update()
            pygame.time.wait(2000)
            first_game = False

        pl.move()
        pr.move()
        cr.move(pl, pr)
        li.spawn()
        sr.spawn(pr.score)
        sl.spawn(pl.score)

        if cr.x + cr.radius >= sc_width:  # Game over
            pygame.time.wait(1000)
            pr = Player('right')
            pl.y = sc_height // 2 - pl.height // 2
            cr = Ball()
            first_game = True
        elif cr.x - cr.radius <= 0:
            pygame.time.wait(1000)
            pl = Player('left')
            pr.y = sc_height // 2 - pr.height // 2
            cr = Ball()
            first_game = True

        pygame.display.update()
        screen.fill((0, 0, 0))
        pygame.time.Clock().tick(120)

pygame.quit()
